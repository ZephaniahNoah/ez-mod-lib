var Opcodes = Java.type("org.objectweb.asm.Opcodes");
var VarInsnNode = Java.type("org.objectweb.asm.tree.VarInsnNode");
var MethodInsnNode = Java.type("org.objectweb.asm.tree.MethodInsnNode");

function initializeCoreMod() {
    return {
        'ezmodlib': {
            'target': {
                'type': 'CLASS',
                'name': 'net.minecraftforge.client.model.ModelLoaderRegistry'
            },
            'transformer': function (classNode) {
                var methods = classNode.methods;
                for (m in methods) {
                    var method = methods[m];
                    if (method.name === "onModelLoadingStart") {
                        var instructions = method.instructions;
                        var firstInstruction = instructions.get(0);

                        // Method
                        var injectModelMethod = new MethodInsnNode(Opcodes.INVOKESTATIC, "com/zephaniahnoah/ezmodlib/ModelInjector", "init", "()V", false);
                        instructions.insertBefore(firstInstruction, injectModelMethod);
                        break;
                    }
                }
                return classNode;
            }
        }
    }
}
