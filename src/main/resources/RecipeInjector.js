var Opcodes = Java.type("org.objectweb.asm.Opcodes");
var VarInsnNode = Java.type("org.objectweb.asm.tree.VarInsnNode");
var MethodInsnNode = Java.type("org.objectweb.asm.tree.MethodInsnNode");

function initializeCoreMod() {
    return {
        'ezmodlib': {
            'target': {
                'type': 'CLASS',
                'name': 'net.minecraft.item.crafting.RecipeManager'
            },
            'transformer': function (classNode) {
                var methods = classNode.methods;
                for (m in methods) {
                    var method = methods[m];
                    if (method.name === "apply" || method.name === "func_212853_a_") {
                        var instructions = method.instructions;
                        var firstInstruction = instructions.get(0);

                        // Parameters
                        instructions.insertBefore(firstInstruction, new VarInsnNode(Opcodes.ALOAD, 1));
                        instructions.insertBefore(firstInstruction, new VarInsnNode(Opcodes.ALOAD, 2));
                        instructions.insertBefore(firstInstruction, new VarInsnNode(Opcodes.ALOAD, 3));
                        //instructions.insertBefore(firstInstruction, new VarInsnNode(Opcodes.ILOAD, 4)); // int

                        // Method
                        var injectRecipeMethod = new MethodInsnNode(Opcodes.INVOKESTATIC, "com/zephaniahnoah/ezmodlib/recipe/RecipeInjector", "injectRecipeManager", "(Ljava/util/Map;Lnet/minecraft/resources/IResourceManager;Lnet/minecraft/profiler/IProfiler;)V", false);
                        instructions.insertBefore(firstInstruction, injectRecipeMethod);
                        break;
                    }
                }
                return classNode;
            }
        }
    }
}
