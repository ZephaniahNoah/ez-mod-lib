var Opcodes = Java.type("org.objectweb.asm.Opcodes");
var VarInsnNode = Java.type("org.objectweb.asm.tree.VarInsnNode");
var MethodInsnNode = Java.type("org.objectweb.asm.tree.MethodInsnNode");

function initializeCoreMod() {
    return {
        'ezmodlib': {
            'target': {
                'type': 'CLASS',
                'name': 'net.minecraft.tags.TagRegistry'
            },
            'transformer': function (classNode) {
                var methods = classNode.methods;
                for (m in methods) {
                    var method = methods[m];
                    if (method.name === "reinjectOptionalTags") {
                        var instructions = method.instructions;
                        var firstInstruction = instructions.get(0);
                        
                        // Parameters
                        instructions.insertBefore(firstInstruction, new VarInsnNode(Opcodes.ALOAD, 1));

                        // Method
                        var injectTagMethod = new MethodInsnNode(Opcodes.INVOKESTATIC, "com/zephaniahnoah/ezmodlib/TagInjector", "reinjectOptionalTags", "(Lnet/minecraft/tags/ITagCollection;)Lnet/minecraft/tags/ITagCollection;", false);
                        instructions.insertBefore(firstInstruction, injectTagMethod);
                        break;
                    }
                }
                return classNode;
            }
        }
    }
}
