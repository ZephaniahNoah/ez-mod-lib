package com.zephaniahnoah.ezmodlib;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import cpw.mods.modlauncher.api.INameMappingService.Domain;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.Item;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ITag.INamedTag;
import net.minecraft.tags.ITagCollection;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.registries.IForgeRegistryEntry;

// What a mess... But it works.
@SuppressWarnings({ "unchecked", "rawtypes" })
public class TagInjector<T extends IForgeRegistryEntry> {
	public static final TagInjector<Item> items = new TagInjector<Item>();
	public static final TagInjector<Block> blocks = new TagInjector<Block>();
	public static final TagInjector<Fluid> fluids = new TagInjector<Fluid>();
	public static final TagInjector<EntityType> entities = new TagInjector<EntityType>();
	private final Map<String, List<T>> tagsToInject = new HashMap<String, List<T>>();
	private static final String tagFieldName = ObfuscationReflectionHelper.remapName(Domain.FIELD, "field_232942_b_");// "tag"

	// Example TagInjector.items.inject("minecraft:coals", Items.APPLE);
	public void inject(String tag, T t) {
		if (t == null)
			return;
		if (tagsToInject.get(tag) != null) {
			tagsToInject.get(tag).add(t);
		} else {
			List list = new ArrayList<T>();
			list.add(t);
			tagsToInject.put(tag, list);
		}
	}

	public static ITagCollection<?> reinjectOptionalTags(ITagCollection<?> tagCollection) {
		for (Entry<ResourceLocation, ?> v : tagCollection.getAllTags().entrySet()) {
			// No need to resolve. Assuming tagCollection is only made of Tag
			attemptInjection(v.getKey(), (ITag<?>) v.getValue());
		}

		for (INamedTag<?> v : ItemTags.getWrappers()) {
			// Should all be INamedTags
			resolveTrueTag(v.getName(), v);
		}
		return tagCollection;
	}

	private static void resolveTrueTag(ResourceLocation name, INamedTag<?> v) {
		Field f;
		try {
			f = v.getClass().getDeclaredField(tagFieldName);
		} catch (Exception ex) {
			try {
				f = v.getClass().getSuperclass().getDeclaredField(tagFieldName);
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
		f.setAccessible(true);
		try {
			ITag<?> tag = (ITag<?>) f.get(v);
			if (tag != null) {
				attemptInjection(name, (ITag<?>) tag);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void attemptInjection(ResourceLocation name, ITag<?> tag) {
		if (items.tagsToInject.containsKey(name.toString())) {
			items.inject(name, (ITag<Item>) tag);
		} else if (blocks.tagsToInject.containsKey(name.toString())) {
			blocks.inject(name, (ITag<Block>) tag);
		} else if (fluids.tagsToInject.containsKey(name.toString())) {
			fluids.inject(name, (ITag<Fluid>) tag);
		} else if (entities.tagsToInject.containsKey(name.toString())) {
			entities.inject(name, (ITag<EntityType>) tag);
		}
	}

	private void inject(ResourceLocation name, ITag<T> itag) {
		for (Entry<String, List<T>> set : tagsToInject.entrySet()) {
			if (set.getKey().equals(name.toString())) {
				for (T i : set.getValue()) {
					try {
						Field field = itag.getClass().getDeclaredField(ObfuscationReflectionHelper.remapName(Domain.FIELD, "field_241282_b_"));// valuesList
						field.setAccessible(true);

						List<T> oldValues = itag.getValues();
						List<T> newValues = new ArrayList<T>();
						if (!oldValues.contains(i)) {
							for (T it : oldValues) {
								newValues.add(it);
							}
							newValues.add(i);

							field.set(itag, ImmutableList.copyOf(newValues));
							field = itag.getClass().getDeclaredField(ObfuscationReflectionHelper.remapName(Domain.FIELD, "field_241283_c_"));// values
							field.setAccessible(true);
							field.set(itag, ImmutableSet.copyOf(newValues));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}
