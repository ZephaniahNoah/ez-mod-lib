package com.zephaniahnoah.ezmodlib.recipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import com.google.gson.JsonObject;
import com.mojang.datafixers.util.Pair;

import net.minecraft.util.ResourceLocation;

public abstract class CraftingRecipe extends Recipe {

	protected Map<Character, List<Pair<String, String>>> keyIngredients = new HashMap<Character, List<Pair<String, String>>>();
	protected static final String item = "item";
	protected static final String tag = "tag";
	protected static final char no = ' ';
	private int count;

	public CraftingRecipe(ResourceLocation rl, Supplier<String> result, int count) {
		super(rl, result);
		this.count = count;
	}

	protected void tag(char ind, String id, String type) {
		Pair<String, String> pair = new Pair<String, String>(type, id);
		if (keyIngredients.containsKey(ind)) {
			keyIngredients.get(ind).add(pair);
		} else {
			List<Pair<String, String>> list = new ArrayList<Pair<String, String>>();
			list.add(pair);
			keyIngredients.put(ind, list);
		}
	}

	@Override
	protected void buildPost(JsonObject el, JsonObject resultItem) {
		resultItem.addProperty("count", count);
		super.buildPost(el, resultItem);
	}

	@Override
	protected abstract void build();
}