package com.zephaniahnoah.ezmodlib.recipe;

import java.util.function.Supplier;

import com.google.gson.JsonObject;
import com.zephaniahnoah.ezmodlib.EzModLib;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class SmithingRecipe extends Recipe {

	private Supplier<String> base;
	private Supplier<String> addition;

	public SmithingRecipe(ResourceLocation rl, String base, String addition, String result) {
		this(rl, () -> base, () -> addition, () -> result);
	}

	public SmithingRecipe(ResourceLocation rl, Item base, Item addition, Item result) {
		this(rl, () -> base.getRegistryName().toString(), () -> addition.getRegistryName().toString(), () -> result.getRegistryName().toString());
	}

	public SmithingRecipe(ResourceLocation rl, Supplier<String> base, Supplier<String> addition, Supplier<String> result) {
		super(rl, result);
		this.base = base;
		this.addition = addition;
	}

	@Override
	public void build() {
		JsonObject el = new JsonObject();
		el.addProperty("type", "minecraft:smithing");
		if (group != null) {
			el.addProperty("group", group);
		}

		// Base
		JsonObject baseItem = new JsonObject();
		baseItem.addProperty("item", base.get());
		el.add("base", baseItem);

		// Addition
		JsonObject additionItem = new JsonObject();
		additionItem.addProperty("item", addition.get());
		el.add("addition", additionItem);

		// Result
		JsonObject resultItem = new JsonObject();
		resultItem.addProperty("item", result.get().toString());
		el.add("result", resultItem);

		EzModLib.recipes.put(resourceLocation, el);
	}
}