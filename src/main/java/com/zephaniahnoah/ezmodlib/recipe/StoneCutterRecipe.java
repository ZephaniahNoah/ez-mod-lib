package com.zephaniahnoah.ezmodlib.recipe;

import java.util.function.Supplier;

import com.google.gson.JsonObject;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class StoneCutterRecipe extends SingleInputRecipe {

	private int count;

	public StoneCutterRecipe(ResourceLocation rl, String in, String out, int count) {
		this(rl, () -> in, () -> out, count);
	}

	public StoneCutterRecipe(ResourceLocation rl, Item in, Item out, int count) {
		this(rl, () -> in.getRegistryName().toString(), () -> out.getRegistryName().toString(), count);
	}

	public StoneCutterRecipe(ResourceLocation rl, Supplier<String> in, Supplier<String> out, int count) {
		super(rl, SingleInputType.STONECUTTING, in, out);
		this.count = count;
	}

	@Override
	protected void build() {
		JsonObject el = new JsonObject();
		el.addProperty("count", count);
		buildPost(el, null);
	}
}