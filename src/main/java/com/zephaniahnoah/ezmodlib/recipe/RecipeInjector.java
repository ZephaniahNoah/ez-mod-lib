package com.zephaniahnoah.ezmodlib.recipe;

import java.util.Map;

import com.google.gson.JsonElement;
import com.zephaniahnoah.ezmodlib.EzModLib;

import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;

public final class RecipeInjector {
	public static void injectRecipeManager(Map<ResourceLocation, JsonElement> objectIn, IResourceManager resourceManagerIn, IProfiler profilerIn) {
		if (EzModLib.recipes.isEmpty()) {
			for (Recipe r : EzModLib.unbuiltRecipes) {
				r.build();
			}
			EzModLib.unbuiltRecipes.clear();
		}
		objectIn.putAll(EzModLib.recipes);
	}
}