package com.zephaniahnoah.ezmodlib.recipe;

import java.util.function.Supplier;

import com.google.gson.JsonObject;
import com.zephaniahnoah.ezmodlib.EzModLib;

import net.minecraft.util.ResourceLocation;

public abstract class SingleInputRecipe extends Recipe {

	private SingleInputType type;
	private Supplier<String> ingredient;

	public SingleInputRecipe(ResourceLocation rl, SingleInputType type, Supplier<String> in, Supplier<String> out) {
		super(rl, in);
		this.type = type;
		this.ingredient = in;
		result = out;
	}

	@Override
	protected void buildPost(JsonObject el, JsonObject resultItem) {
		el.addProperty("type", "minecraft:" + type.name().toLowerCase());
		JsonObject item = new JsonObject();
		el.add("ingredient", item);
		item.addProperty("item", ingredient.get().toString());
		el.addProperty("result", result.get().toString());
		EzModLib.recipes.put(resourceLocation, el);
	}

	public enum SingleInputType {
		BLASTING, SMELTING, SMOKING, STONECUTTING
	}
}