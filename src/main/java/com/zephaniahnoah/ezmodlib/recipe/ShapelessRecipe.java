package com.zephaniahnoah.ezmodlib.recipe;

import java.util.List;
import java.util.function.Supplier;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mojang.datafixers.util.Pair;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class ShapelessRecipe extends CraftingRecipe {

	public ShapelessRecipe(ResourceLocation rl, String result, int count) {
		super(rl, () -> result, count);
	}

	public ShapelessRecipe(ResourceLocation rl, Item result, int count) {
		super(rl, () -> result.getRegistryName().toString(), count);
	}

	public ShapelessRecipe(ResourceLocation rl, Supplier<String> result, int count) {
		super(rl, result, count);
	}

	public ShapelessRecipe tag(String id) {
		tag(no, id, tag);
		return this;
	}

	public ShapelessRecipe item(String id) {
		tag(no, id, item);
		return this;
	}

	@Override
	public void build() {
		JsonObject el = new JsonObject();
		el.addProperty("type", "minecraft:crafting_shapeless");
		JsonArray ingredients = new JsonArray();
		for (List<Pair<String, String>> l : keyIngredients.values()) {
			Pair<String, String> s = l.get(0);
			JsonObject ingredient = new JsonObject();
			ingredient.addProperty(s.getFirst(), s.getSecond());
			ingredients.add(ingredient);
		}
		el.add("ingredients", ingredients);
		buildPost(el, new JsonObject());
	}
}