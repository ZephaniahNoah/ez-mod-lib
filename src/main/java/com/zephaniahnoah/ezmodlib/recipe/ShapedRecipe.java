package com.zephaniahnoah.ezmodlib.recipe;

import java.util.List;
import java.util.Map.Entry;
import java.util.function.Supplier;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mojang.datafixers.util.Pair;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class ShapedRecipe extends CraftingRecipe {

	private String[] recipe;

	public ShapedRecipe(ResourceLocation rl, String[] recipe, String result, int count) {
		this(rl, recipe, () -> result, count);
	}

	public ShapedRecipe(ResourceLocation rl, String[] recipe, Item result, int count) {
		this(rl, recipe, () -> result.getRegistryName().toString(), count);
	}

	public ShapedRecipe(ResourceLocation rl, String[] recipe, Supplier<String> result, int count) {
		super(rl, result, count);
		this.recipe = recipe;
	}

	public ShapedRecipe tag(char ind, String id) {
		tag(ind, id, tag);
		return this;
	}

	public ShapedRecipe item(char ind, String id) {
		tag(ind, id, item);
		return this;
	}

	@Override
	public void build() {
		JsonObject el = new JsonObject();
		el.addProperty("type", "minecraft:crafting_shaped");
		JsonArray ingredients = new JsonArray();
		for (String s : recipe) {
			ingredients.add(s);
		}
		el.add("pattern", ingredients);
		JsonObject keys = new JsonObject();
		for (Entry<Character, List<Pair<String, String>>> ing : keyIngredients.entrySet()) {
			JsonArray ingredientArray = new JsonArray();
			for (Pair<String, String> g : ing.getValue()) {
				JsonObject obj = new JsonObject();
				obj.addProperty(g.getFirst(), g.getSecond());
				ingredientArray.add(obj);
			}
			keys.add(ing.getKey().toString(), ingredientArray);
		}
		el.add("key", keys);
		buildPost(el, new JsonObject());
	}
}