package com.zephaniahnoah.ezmodlib.recipe;

import java.util.function.Supplier;

import com.google.gson.JsonObject;
import com.zephaniahnoah.ezmodlib.EzModLib;

import net.minecraft.util.ResourceLocation;

public abstract class Recipe {

	protected ResourceLocation resourceLocation;
	protected Supplier<String> result;
	public String group;

	public Recipe(ResourceLocation rl, Supplier<String> result) {
		resourceLocation = rl;
		this.result = result;
		EzModLib.unbuiltRecipes.add(this);
	}

	protected abstract void build();

	protected void buildPost(JsonObject el, JsonObject resultItem) {
		if (group != null) {
			el.addProperty("group", group);
		}
		resultItem.addProperty("item", result.get());
		el.add("result", resultItem);
		EzModLib.recipes.put(resourceLocation, el);
	}
}