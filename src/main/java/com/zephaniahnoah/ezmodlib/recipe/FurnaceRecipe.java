package com.zephaniahnoah.ezmodlib.recipe;

import java.util.function.Supplier;

import com.google.gson.JsonObject;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class FurnaceRecipe extends SingleInputRecipe {

	private int time;
	private double xp;

	public FurnaceRecipe(ResourceLocation rl, SingleInputType type, String in, String out, int time, double xp) {
		this(rl, type, () -> in, () -> out, time, xp);
	}

	public FurnaceRecipe(ResourceLocation rl, SingleInputType type, Item in, Item out, int time, double xp) {
		this(rl, type, () -> in.getRegistryName().toString(), () -> out.getRegistryName().toString(), time, xp);
	}

	public FurnaceRecipe(ResourceLocation rl, SingleInputType type, Supplier<String> in, Supplier<String> out, int time, double xp) {
		super(rl, type, in, out);
		this.time = time;
		this.xp = xp;
	}

	@Override
	protected void build() {
		JsonObject el = new JsonObject();
		el.addProperty("experience", xp);
		el.addProperty("cookingtime", time);
		buildPost(el, null);
	}
}