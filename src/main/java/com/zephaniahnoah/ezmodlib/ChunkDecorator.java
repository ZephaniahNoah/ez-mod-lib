package com.zephaniahnoah.ezmodlib;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.ChunkDataEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public abstract class ChunkDecorator {
    public static final String tag = "EzModLibGenTag";
    private static final HashSet<ChunkPos> markedForTagging = new HashSet<ChunkPos>();
    public static List<ChunkDecorator> decorators = new ArrayList<ChunkDecorator>();

    protected abstract void decorate(IChunk chunk, Random rand);

    public static void dec(IChunk chunk) {
        for (ChunkDecorator dec : decorators) {
            dec.decorate(chunk, EzModLib.rand);
        }
        markedForTagging.add(chunk.getPos());
    }

    static {
        MinecraftForge.EVENT_BUS.register(new Object() {
            @SubscribeEvent
            public void onChunkDataLoad(ChunkDataEvent.Load e) {
                if (e.getWorld() instanceof ServerWorld) {
                    try {
                        CompoundNBT data = e.getData();
                        if (!data.contains(tag)) {
                            dec(e.getChunk());
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @SubscribeEvent(priority = EventPriority.LOWEST)
            public void onChunkLoad(ChunkEvent.Load e) {
                if (e.getWorld() instanceof ServerWorld) {
                    try {
                        CompoundNBT nbt = ((ServerWorld) e.getChunk().getWorldForge()).getChunkSource().chunkMap.read(e.getChunk().getPos());
                        if (nbt == null || !nbt.contains(ChunkDecorator.tag)) {
                            ChunkDecorator.dec(e.getChunk());
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @SubscribeEvent
            public void onChunkDataSave(ChunkDataEvent.Save e) {
                if (e.getWorld() instanceof ServerWorld) {
                    if (markedForTagging.contains(e.getChunk().getPos())) {
                        e.getData().putBoolean(tag, false);
                        markedForTagging.remove(e.getChunk().getPos());
                    } else try {
                        CompoundNBT nbt = ((ServerWorld) e.getWorld()).getChunkSource().chunkMap.read(e.getChunk().getPos());
                        if (nbt != null) {
                            if (nbt.contains(tag)) {
                                e.getData().putBoolean(tag, false);
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }
}