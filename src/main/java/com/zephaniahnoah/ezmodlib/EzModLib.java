package com.zephaniahnoah.ezmodlib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;

import com.google.gson.JsonElement;
import com.mojang.datafixers.util.Pair;
import com.zephaniahnoah.ezmodlib.recipe.Recipe;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EntityType.Builder;
import net.minecraft.entity.EntityType.IFactory;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

// TODO: Should only set registers if used

// TODO: Replace lang system
// TODO: Check for existing resources before injecting
// TODO: BlockState injecting
// TODO: Add texture generation system.
@Mod("ezmodlib")
public class EzModLib {
	public static final Map<String, DeferredRegister<Item>> itemRegisters = new HashMap<String, DeferredRegister<Item>>();
	public static final Map<String, DeferredRegister<Block>> blockRegisters = new HashMap<String, DeferredRegister<Block>>();
	public static final Map<String, DeferredRegister<TileEntityType<?>>> tileEntityRegisters = new HashMap<String, DeferredRegister<TileEntityType<?>>>();
	public static final Map<String, DeferredRegister<EntityType<?>>> entityRegisters = new HashMap<String, DeferredRegister<EntityType<?>>>();
	public static final Map<ResourceLocation, JsonElement> recipes = new HashMap<ResourceLocation, JsonElement>();
	public static List<Recipe> unbuiltRecipes = new ArrayList<Recipe>();
	public static final Map<Block, String> blockModels = new HashMap<Block, String>();
	public static final Map<Item, String> itemModels = new HashMap<Item, String>();
	public static final Map<String, Pair<IRenderFactory, RegistryObject>> renderers = new HashMap<String, Pair<IRenderFactory, RegistryObject>>();
	// public static final Map<String, EntityType> entities = new HashMap<String, EntityType>();
	public static Random rand = new Random();
	public static final String DONT_INJECT = "DONT_INJECT";
	public static final String INJECT_EXISTING_MODEL = "INJECT_EXISTING_MODEL";

	public EzModLib() {
		FMLJavaModLoadingContext.get().getModEventBus().register(this);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SubscribeEvent
	public void clientSetup(FMLClientSetupEvent e) {
		for (Pair<IRenderFactory, RegistryObject> set : renderers.values()) {
			RenderingRegistry.registerEntityRenderingHandler((EntityType) set.getSecond().get(), set.getFirst());
		}
	}

	public static void register(String modid, String name, Item item) {
		register(modid, name, item, null);
	}

	public static void register(String modid, String name, Item item, String jsonModel) {
		itemModels.put(item, jsonModel);
		itemRegisters.get(modid).register(name, () -> item);
	}

	public static void register(String modid, String name, Block block) {
		register(modid, name, block, null, null, null);
	}

	public static void register(String modid, String name, Block block, String jsonModel, Item i, String itemModel) {
		registerBlockOnly(modid, name, block, jsonModel);
		register(modid, name, i == null ? new BlockItem(block, new Item.Properties()) : i, itemModel == null ? "{\"parent\": \"" + modid + ":block/" + name + "\"}" : itemModel);
	}

	public static void registerBlockOnly(String modid, String name, Block block, String jsonModel) {
		blockModels.put(block, jsonModel);
		blockRegisters.get(modid).register(name, () -> block);
	}

	public static Pair<Builder<?>, RegistryObject<?>> register(String modid, String name, IFactory<?> entity, EntityClassification classification) {
		Builder<?> builder = EntityType.Builder.of(entity, classification);
		RegistryObject<?> regObj = entityRegisters.get(modid).register(name, () -> builder.build(name));
		return new Pair<Builder<?>, RegistryObject<?>>(builder, regObj);
	}

	@OnlyIn(Dist.CLIENT)
	public static void registerRender(IRenderFactory renderer, String name, RegistryObject regObj) {
		renderers.put(name, new Pair<IRenderFactory, RegistryObject>(renderer, regObj));
	}

	public static RegistryObject<TileEntityType<?>> register(String modid, String name, Supplier<? extends TileEntity> tileEntity, Block block) {
		return tileEntityRegisters.get(modid).register(name, () -> TileEntityType.Builder.of(tileEntity, block).build(null));
	}

	public static void init(String modid) {
		itemRegisters.put(modid, DeferredRegister.create(ForgeRegistries.ITEMS, modid)); // WTF Java??? HashMap.put() returns the last value.. which is null in this case. No one liners here :'C
		itemRegisters.get(modid).register(FMLJavaModLoadingContext.get().getModEventBus());
		blockRegisters.put(modid, DeferredRegister.create(ForgeRegistries.BLOCKS, modid));
		blockRegisters.get(modid).register(FMLJavaModLoadingContext.get().getModEventBus());
		entityRegisters.put(modid, DeferredRegister.create(ForgeRegistries.ENTITIES, modid));
		entityRegisters.get(modid).register(FMLJavaModLoadingContext.get().getModEventBus());
		tileEntityRegisters.put(modid, DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, modid));
		tileEntityRegisters.get(modid).register(FMLJavaModLoadingContext.get().getModEventBus());
	}
}