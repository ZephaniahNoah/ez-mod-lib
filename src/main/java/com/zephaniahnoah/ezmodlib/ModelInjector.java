package com.zephaniahnoah.ezmodlib;

import java.io.InputStream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.IOUtils;

import com.google.gson.JsonObject;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.model.BlockModel;
import net.minecraft.client.renderer.model.IUnbakedModel;
import net.minecraft.client.renderer.model.ModelBakery;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.TieredItem;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class ModelInjector {
	private static Set<ResourceLocation> loadingStack;
	private static Map<ResourceLocation, IUnbakedModel> unbakedCache;

	public static void init() {
		ModelInjector.unbakedCache = ObfuscationReflectionHelper.getPrivateValue(ModelBakery.class, ModelLoader.instance(), "field_217849_F");// unbakedCache
		ModelInjector.loadingStack = ObfuscationReflectionHelper.getPrivateValue(ModelBakery.class, ModelLoader.instance(), "field_217848_D");// loadingStack
		for (Entry<Item, String> entry : EzModLib.itemModels.entrySet())
			model(entry.getKey(), entry.getValue());
		for (Entry<Block, String> entry : EzModLib.blockModels.entrySet())
			model(entry.getKey(), entry.getValue());
	}

	private static void model(ForgeRegistryEntry<?> value, String json) {
		if (json == EzModLib.DONT_INJECT)
			return;
		ResourceLocation itemResourceLocation = value.getRegistryName();
		if (json == EzModLib.INJECT_EXISTING_MODEL)
			json = getExistingModel(itemResourceLocation);
		boolean block = value instanceof Block;
		ResourceLocation resourceLocation = new ResourceLocation(itemResourceLocation.getNamespace(), (block ? "block" : "item") + "/" + itemResourceLocation.getPath());
		BlockModel model = BlockModel.fromString(json == null ? json(value, resourceLocation.toString(), block) : json);
		ModelResourceLocation modelresourcelocation = new ModelResourceLocation(itemResourceLocation + "#" + (block ? "" : "inventory"));
		unbakedCache.put(modelresourcelocation, model);
		loadingStack.addAll(model.getDependencies());
		unbakedCache.put(resourceLocation, model);
	}

	private static String getExistingModel(ResourceLocation res) {
		try (InputStream in = ModelInjector.class.getResourceAsStream("/assets/" + res.getNamespace() + "/models/block/" + res.getPath() + ".json")) {
			return IOUtils.toString(in, "UTF-8");
		} catch (Exception e) {
			System.out.println("Failed to load " + res);
			e.printStackTrace();
			return null;
		}
	}

	private static String json(ForgeRegistryEntry<?> item, String textureName, boolean block) {
		JsonObject model = new JsonObject();
		JsonObject layer = new JsonObject();
		layer.addProperty(block ? "all" : "layer0", textureName);
		model.addProperty("parent", block ? "minecraft:block/cube_all" : "minecraft:item/" + (item instanceof TieredItem ? "handheld" : "generated"));
		model.add("textures", layer);
		return model.toString();
	}
}