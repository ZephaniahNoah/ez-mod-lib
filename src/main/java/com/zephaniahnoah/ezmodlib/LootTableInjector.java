package com.zephaniahnoah.ezmodlib;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import net.minecraft.loot.LootTable;
import net.minecraft.loot.LootTableManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

public final class LootTableInjector {

	private static final String tablesName = "field_186527_c";
	public static Map<ResourceLocation, JsonElement> tablesToInject = new HashMap<ResourceLocation, JsonElement>();

	public static void injectLootTableManager(LootTableManager manager) {

		Map<ResourceLocation, LootTable> lootTables = new HashMap<>(ObfuscationReflectionHelper.getPrivateValue(LootTableManager.class, manager, tablesName));

		Gson GSON = ObfuscationReflectionHelper.getPrivateValue(LootTableManager.class, null, "field_186526_b");// GSON

		for (Entry<ResourceLocation, JsonElement> entry : tablesToInject.entrySet()) {
			LootTable lootTable = ForgeHooks.loadLootTable(GSON, entry.getKey(), entry.getValue(), true, manager);
			lootTables.put(entry.getKey(), lootTable);
		}

		ObfuscationReflectionHelper.setPrivateValue(LootTableManager.class, manager, lootTables, tablesName);
	}
}