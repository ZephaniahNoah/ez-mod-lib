package com.zephaniahnoah.ezmodlib.util;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

// A wrapper for any Map which allows you to get a copy of itself but with the keys and values swapped.
public class SwapMap<A, B> implements Map<A, B> {

	private final Map<A, B> internalMap;
	private Map<B, A> swap;

	public SwapMap(Map<A, B> map) {
		internalMap = map;
	}

	// Make sure you call reset() if you modify the internal map
	public Map<A, B> getInternalMap() {
		return internalMap;
	}

	public Map<B, A> swap() {
		return swap == null ? swap = internalMap.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey)) : swap;
	}

	private void reset() {
		swap = null;
		swap();
	}

	@Override
	public int size() {
		return internalMap.size();
	}

	@Override
	public boolean isEmpty() {
		return internalMap.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return internalMap.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return internalMap.containsValue(value);
	}

	@Override
	public B get(Object key) {
		return internalMap.get(key);
	}

	@Override
	public B put(A key, B value) {
		B map = internalMap.put(key, value);
		reset();
		return map;
	}

	@Override
	public B remove(Object key) {
		B map = internalMap.remove(key);
		reset();
		return map;
	}

	@Override
	public void putAll(Map<? extends A, ? extends B> m) {
		internalMap.putAll(m);
		reset();
	}

	@Override
	public void clear() {
		internalMap.clear();
		reset();
	}

	@Override
	public Set<A> keySet() {
		return internalMap.keySet();
	}

	@Override
	public Collection<B> values() {
		return internalMap.values();
	}

	@Override
	public Set<Entry<A, B>> entrySet() {
		return internalMap.entrySet();
	}
}